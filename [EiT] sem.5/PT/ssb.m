%MATLAB example for SSB -- EE-4021, 9/23/11
fs = 1e5; %sampling frequency
t = -0.05:1/fs:0.05; %time vector
m = sinc(1000*t); %sinc(1000*pi*t) - different syntax
mh = (cos(1000*pi*t)-1)./(1000*pi*t); %Hilbert transform for SSB
mh(5001) = 0; %mh(0) = 0 in the limit
%Define USB, SSB, and DSB-SC signals
usb = m.*cos(2*pi*10000*t)-mh.*sin(2*pi*10000*t);
lsb = m.*cos(2*pi*10000*t)+mh.*sin(2*pi*10000*t);
dsb = m.*cos(2*pi*10000*t);
%plot vs. time
figure(1)
%plot(t,usb,'b',t,lsb,'r',t,dsb,'g')
plot(t,lsb)
xlabel('t'), title('USB, LSB, DSB-SC signals'), legend('USB','LSB','DSB-SC')