Hd(1) = design(Hf,'window','window',@hamming);
Hd(2) = design(Hf,'window','window',{@chebwin,50});
hfvt = fvtool(Hd,'Color','White');
legend(hfvt,'Hamming window design','Dolph-Chebyshev window design')