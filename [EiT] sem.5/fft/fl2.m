clear all; 
clc; 
N = 32; 
k = 1 : 1 : N; 



%sygnal sprobkowany 
x(1) = 0; 
x(2) = 0.3; 
x(3) = 0.6; 
x(4) = 0.9; 
x(5) = 0.6; 
x(6) = 0.3; 
x(7) = 0; 
x(8) = -0.3; 
x(9) = -0.6; 
x(10) = -0.9; 
x(11) = -0.6; 
x(12) = -0.3; 
x(13) = 0; 
x(14) = 0.3; 
x(15) = 0.6; 
x(16) = 0.9; 
x(17) = 0.6; 
x(18) = 0.3; 
x(19) = 0; 
x(20) = -0.3; 
x(21) = -0.6; 
x(22) = -0.9; 
x(23) = -0.6; 
x(24) = -0.3; 
x(25) = 0; 
x(26) = 0.3; 
x(27) = 0.6; 
x(28) = 0.9; 
x(29) = 0.6; 
x(30) = 0.3; 
x(31) = 0; 
x(32) = -0.3; 

%kopia sygnalu 
xreal(1) = 0; 
xreal(2) = 0.3; 
xreal(3) = 0.6; 
xreal(4) = 0.9; 
xreal(5) = 0.6; 
xreal(6) = 0.3; 
xreal(7) = 0; 
xreal(8) = -0.3; 
xreal(9) = -0.6; 
xreal(10) = -0.9; 
xreal(11) = -0.6; 
xreal(12) = -0.3; 
xreal(13) = 0; 
xreal(14) = 0.3; 
xreal(15) = 0.6; 
xreal(16) = 0.9; 
xreal(17) = 0.6; 
xreal(18) = 0.3; 
xreal(19) = 0; 
xreal(20) = -0.3; 
xreal(21) = -0.6; 
xreal(22) = -0.9; 
xreal(23) = -0.6; 
xreal(24) = -0.3; 
xreal(25) = 0; 
xreal(26) = 0.3; 
xreal(27) = 0.6; 
xreal(28) = 0.9; 
xreal(29) = 0.6; 
xreal(30) = 0.3; 
xreal(31) = 0; 
xreal(32) = -0.3; 

xc = x; 

subplot(4,1,1);stem(k,x);  title('Sygnal sprobkowany'); 

%Przestawienie kolejno�ci pr�bek 
a = 1; 
for b = 1 : N-1 
        if (b < a) 
            T = x(a); 
            Treal = xreal(a); 
            Timg  = 0; 
            
            x(a) = x(b); 
            xreal(a) = xreal(b); 
            ximg(a)  = 0; 
            
            x(b) = T; 
            xreal(b) = Treal; 
            ximg(b)  = 0; 
            
        end 
        c = N/2; 
        while (c < a) 
            a = a - c; c = c/2; 
        end 
        a = a + c; 
end 
ximg(1) = 0; 
ximg(32)= 0; 


%LiczenieFFT    
for e = 1 : log2(N) 
   L = 2^e; 
   M = 2^(e-1); 
    
   Wireal = 1; 
   Wiimg  = 0; 
   Wi = 1; 
    
   Wreal = cos(2*pi/L); 
   Wimg  = -sin(2*pi/L); 
  
   W = cos(2*pi/L)-j*sin(2*pi/L); 
    
   for m = 1 : M 
       for g = m : L : N 
           d = g + M; 
            
           Treal = xreal(d)*Wireal - ximg(d)*Wiimg; 
           Timg  = xreal(d)*Wiimg  + Wireal*ximg(d); 
           T = x(d)*Wi; 
            
           xreal(d) = xreal(g) - Treal; 
           ximg(d)  = ximg(g)  - Timg; 
           x(d) = x(g) - T; 
            
           xreal(g) = xreal(g) + Treal; 
           ximg(g)  = ximg(g)  + Timg; 
           x(g) = x(g) + T; 
            
           
           
           
     TI1 = xreal(d) * Wiimg; 
           TI2 = Wireal * ximg(d); 
           TR1 = xreal(d) * Wireal; 
           TR2 = ximg(d)*Wiimg; 
           Timg  = TI1 + TI2; 
           Treal = TR1 - TR2;
           
           %pause 
           %clc 
              
           H(g) = xreal(g) - j*ximg(g); 
           H(d) = xreal(d) - j*ximg(d); 
           X(d) = sqrt((xreal(d))^2 + (ximg(d))^2); 
           X(g) = sqrt((xreal(g))^2 + (ximg(g))^2); 
       end 
       Wireal = Wireal*Wreal - Wiimg*Wimg; 
       Wiimg  = Wireal*Wimg  + Wreal*Wiimg; 
       Wi = Wi * W; 
   end 
end 

%Wykresy 
subplot(4,1,2); 
stem(k,abs(X/(N/2))); grid; title('Moja FFT'); 
subplot(4,1,3); 
stem(k,abs(x/(N/2)));grid; title('Z x-ow'); 
subplot(4,1,4); 
stem(k,abs(fft(xc)/(N/2)));grid;title('Wbudowana');