clear all;
close all;
clc;
N=1024;
i=sqrt(-1);
wn=exp(-i*2*pi/N);

t=0:0.001:1.023;
x=sawtooth(t*100,0.5);
kopiax=x;
figure
plot(t,x);
xlabel('Frequency');
ylabel('Amplitude');
title('Sawtooth signal');


nn=log2(N);

for k=0:1:N-1	
   kcopy=k; index=0;
   for m=1:nn           
      if (rem(k,2)==0) 
         k=k/2;
      else 				
         index=index+2^(nn-m);
         k=(k-1)/2;
      end
   end
   y(index+1)=x(kcopy+1);
end
x=y;                   



for a=log2(N):-1:1
   krok=log2(N)+1-a;
   for b=1:1:2^a/2 wnm(a,b)=wn^0;
   end
   for c=2^a/2+1:1:2^a wnm(a,c)=wn^((c-(2^a/2+1))*2^(krok-1));
   end
   %  KOPIOWANIE
   
   for d=1:1:2^a
      for e=1:1:2^(log2(N)-a)-1 wnm(a,d+e*2^a)=wnm(a,d);
      end
   end
end

%x.*wnm(1,1:1024); WZOR MNOZENIA


% MOTYLKI
for n=0:1:log2(N)-1
x=x.*wnm(n+1,1:1:N);   
   k=0;
   for a=1:1:2^(-n+log2(N)-1)     %BLOKI
      
      for b=1:1:2^n               %ILOSC MOTYLKOW W BLOKU
           
         %motyl(k+b,k+b+2^n)
         tmp=x(k+b);
         x(k+b)=x(k+b)+x(k+b+2^n);     %MOTYL  
         x(k+b+2^n)=-x(k+b+2^n)+tmp;   %MOTYL         
         
         
          end
k=k+2^n+2^n;
      end
   end
   figure
plot(1:1:N,abs(x))
xlabel('Frequency');
ylabel('Power');
  figure
plot(1:1:N,abs(x-(fft(kopiax))))
xlabel('Frequency');
ylabel('Power');
   