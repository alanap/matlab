%Protok� Diffiego-Hellmana
%Protok� uzgadniania kluczy Diffiego-Hellmana � protok� 
%kryptograficzny, w wyniku wykonania kt�rego dwie
%strony otrzymuj� tak� sam� liczb�, przy czym otrzymanie 
%tej liczby na podstawie tre�ci wymienionych wiadomo�ci 
%mi�dzy stronami jest praktycznie niemo�liwe.
%Liczba ta mo�e by� potem u�ywana jako klucz do 
%szyfrowania komunikacji.

%Alice
a=3;
fprintf('Alice get random number \t a=%0.0f \n' ,a)

%Bob
b=7;
fprintf('Bob get random number \t\t b=%0.0f \n' ,b)

%Both
p=226;
g=45;
fprintf('Both of them knows numbers:\t p=%0.0f', p);
fprintf('\n \t \t \t \t \t \t   \t q=%0.0f', g);
fprintf('\n');

%Alice
B=mod((g^b),p);
fprintf('Alice get from Bob: B=(g^b)mod(p)=%0.0f',B);


K=mod((B^a),p)

%Bob
A=mod((g^a),p);
fprintf('Bob get from Alice: A=(g^a)mod(p)=%0.0f',A);


K2=mod((A^b),p)

fprintf('Result:\n');
if K2==K
    fprintf('Succes, output numbers, the same\n');

else
    fprintf('Failed\n');
    
end

