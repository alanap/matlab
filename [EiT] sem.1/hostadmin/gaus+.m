clear;
A=input('podaj macierz:  ');
b=input('podaj wektor ');
[n,n]=size(A);
x=zeros(n,1);
c=zeros(1,1+n);
D=[A b'];
for p=1:n-1;
    [Y,j]=max(abs(D(p:n,p)));
    C=D(p,:);
    D(p,:)=D(j+p-1,:);
    D(j+p-1,:)=C;
    if D(p,p)==0
        disp('A macierz osobliwa brak rozw ');
        break
    end
    for k=p+1:n;
        m=D(k,p)/D(p,p);
        D(k,p:n+1)=D(k,p:n+1)-m*D(p,p:n+1);
    end
end

x(n)=b(n)/D(n,n);

v=D(:,n+1);
for i=n:-1:1
   suma=0;
   for j=i+1:n
      suma=suma+D(i,j)*x(j);
   end
   x(i)=(v(i)-suma)/D(i,i);
end
for i=1:n
disp(x(i));
end
