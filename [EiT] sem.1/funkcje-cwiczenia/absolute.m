%Wartosc bezwzgledna z wektora


function y = absolute_value(v)
n=length(v);

for i=1:n
    if v(i)<0
        y(i)=v(i)*(-1);
    else
        y(i)=v(i);
    end
end


