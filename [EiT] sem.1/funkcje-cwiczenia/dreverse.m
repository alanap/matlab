%Double reverse (n): v i u




function y = multiply_reverse(v,u)
n=length(v);
for i=1:n
    y(i) = v(i)*u(n-i+1);
end
