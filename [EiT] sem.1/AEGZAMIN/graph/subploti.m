%subplot
x=-3:0.1:3;
y=sin(pi*x);
z=cos(pi*2);
w=cos(pi*x).^2;

subplot(2,2,1)
plot(x,y)
subplot(2,2,2)
plot(x,z)
subplot(2,2,3)
plot(x,w)