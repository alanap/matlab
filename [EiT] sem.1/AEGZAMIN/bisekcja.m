%bisekcja

a=input('a ');
b=input('b ');
x=-3:0.1:3;
y=x.^3+x.^2+1;

axis([-2 1 -1 1])
plot(x,y,'r')
grid on


while abs(b-a)>eps
    c=(a+b)/2;

    if funkcja(c)==0
        break
    elseif funkcja(a)*funkcja(c)<0
        b=c
    else
        a=c
    end
end

disp(c)
