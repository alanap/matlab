function y=skalar(U,V)

nU=length(U);
nV=length(V);

if nV~=nU
    disp('error')
end
s=0;
for i=1:nU
    s=s+V(i)*U(i);
end

s
    