function [maximal,minimal]=min_max(V)

n=length(V);

if n<0
    disp('rrror')
end

maximal=V(1);
minimal=V(1);

for i=2:n
    if V(i)>maximal
        maximal=V(i);
    elseif V(i)<minimal
        minimal=V(i);
    end
end
maximal
minimal