%wykres
[x,y]=meshgrid(-3:0.1:3)

z=((x.^2)/6)+((y.^2)/5);
mesh(x,y,z)