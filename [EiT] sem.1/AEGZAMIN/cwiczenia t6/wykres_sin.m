%wykres sin
x=[-3:0.1:3];
n=length(x);

for i=1:n
    if x(i)~=0
        y(i)=((sin(pi*x)).^2)/((pi*x).^2);
    end
end

plot(x,y)