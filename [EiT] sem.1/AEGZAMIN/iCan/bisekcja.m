%Bisekcja
a=input('a: ');
b=input('b: ');
eps=input('epsilon: ');
blad=1;

if funkcja(a)*funkcja(b)>0
    disp('Brak miejsca zerowgo');
    break
end

while abs(b-a)>eps
    c=(a+b)/2;
    if funkcja(c)==0
        break
        disp(c);
    elseif funkcja(a)*funkcja(c)<0
        b=c;
    else
        a=c;
    end
end
c

x=[c-2:0.1:c+2];
y=x.^3+x.^2+1;
plot(x,y)
grid on
xlabel('Argumenty')
ylabel('Wartosci')
