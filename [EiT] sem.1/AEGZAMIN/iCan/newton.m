%Metoda NEWTONA
x=input('Podaj x0= ');
eps=input('eps= ');
iter_max=input('Lp. iteracji= ');
blad=1;
iter=1;

while (blad>eps) & (iter<iter_max)
    x_stare=x;
    x=x-funkcja(x)/pochodna(x);
    iter=iter+1;
    blad=abs((x-x_stare)/x);
end

x
x=[x-2:0.1:x+2];
y=x.^3+x.^2+1;
plot(x,y)
grid on