clear;
clc;
A=[0 1 1; 0 0 1; 1 1 1]
b=[1 5 8]
[n,n]=size(A);
x=zeros(n,1);
c=zeros(1,1+n);
D=[A b'];
for p=1:n-1;
    [Y,j]=max(abs(D(p:n,p)));
    C=D(p,:);
    D(p,:)=D(j+p-1,:);
    D(j+p-1,:)=C;
    if D(p,p)==0
        disp('A macierz osobliwa brak rozw ');
        break
    end
    for k=p+1:n;
        m=D(k,p)/D(p,p);
        D(k,p:n+1)=D(k,p:n+1)-m*D(p,p:n+1);
    end
end
disp(D);



x(n-2)=b(n-2)/D(n-2,n-2);

for i=n-3:-1:1
    suma=0;
    for j=i+1:1:n-2
        suma=suma+D(i,j)*x(j);
    end
    x(i)=(b(i)-suma)/D(i,i);
end
disp('Rozwiazanie ma postac: ')


for i=1:n
    fprintf('x(%0.0f)=%0.0f \n' ,i ,x(n+1-i))
end
