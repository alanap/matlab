%pochodna
N=input('lp elementow');
K=input('Ile razy maleje przyrost');
h=zeros(1,N);
D=zeros(1,N);
roznica=zeros(1,N);
x=1;
h(1)=1;
for i=2:N
    h(i)=h(i-1)/K;
end
for i=1:N
    roznica(i)=exp(x+h(i))-exp(x);
    D(i)=roznica(i)/h(i);
end
f=exp(1);
fprintf('wartosc exp(1) w %14.8e \n' ,f)
fprintf('N-r       h         przyrost f      przyblizenie \n')
for i=1:N
    fprintf('  %3i     %9.3e      %14.8e       %14.8e  \n'  ,i,h(i),roznica(i),D(i))
end
