clear;
clc;
A=[1 1 1; 0 0 1 ; 0 1 1];
b=[1 5 8];

D=[A b']
n=length(b);

for p=1:n-1
    [Y,j]=max(abs(D(p:n,p)));
    C=D(p,:);
    D(p,:)=D(j+p-1,:);
    D(j+p-1,:)=C;
    if D(p,p)==0
        disp('err');
        break
    end
    
    for k=p+1:n
        m=D(k,p)/D(p,p);
        D(k,p:n+1)=D(k,p:n+1)-m*D(p,p:n+1);
    end
end
D
