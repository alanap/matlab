%Jacobi
A=input('podaj A ');
B=input('podaj B ');
n=length(B);
x=ones(n,1);
maxit=100;
eps = 1e-6;

for i=1:maxit
    x0=x;
    for j=1:n
        x(j)=(B(j)-A(j,[1:j-1,j+1:n])*x0([1:j-1,j+1:n]))/A(j,j);
    end
    blad=norm(x-x0);
    if (blad<eps)
        break;
    end
end
disp(x)
disp(i)