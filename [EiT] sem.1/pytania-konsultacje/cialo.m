%Puszczanie ciala z wysokosci


function v=predkosc(hmax,m)
h=hmax:-hmax/100:0;
g=9.8;
v=sqrt(2*g*hmax);
ep1=m*g.*hmax

ep=m*g.*h


ek=ep1-ep



subplot(1,2,1)
hold on
plot(h,ep)
xlabel('wysokosc');
ylabel('Ep');

subplot(1,2,2)
plot(h,ek)
xlabel('wysokosc');
ylabel('Ek');
hold off