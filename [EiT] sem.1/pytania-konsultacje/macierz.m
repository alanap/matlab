%Macierz z ciagiem

function a=zad4(n,m)

a=0;
for i=1:n
    a(i,1)=1;
    for j=1:m-1
        a(i,j+1)=a(i,j)+i;
    end
end
disp(a)
surf(a)
