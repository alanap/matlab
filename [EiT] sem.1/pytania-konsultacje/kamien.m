%Daj kamienia


function s=kamien(v,tmax)

t=0:tmax/100:tmax;
g=9.8;



s= v.*t-g/2.*t.^2;

plot(t,s)
