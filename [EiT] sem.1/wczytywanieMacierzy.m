%Wczytywanie macierzy

w=input('Podaj ilosc wierszy: ');

while w<=0
    disp('Ilosc wierszy musi byc dodatnia')
    w=input('Podaj ilosc wierszy: ');
end

k=input('Podaj ilosc kolumn: ');
while k<=0
    disp('Ilosc kolumn musi byc dodatnia')
    k=input('Podaj ilosc kolumn: ');
end


wart_max=0;
indexw=0;
indexk=0;
%Wprowadzanie wartosci oraz szukanie makxiumum
for i=1:w
    for j=1:k
        a(i,j)=input('Podaj wartosc: ');
        if a(i,j)>wart_max
            wart_max=a(i,j);
            indexw=i;
            indexk=j;
        end
    end
end
disp('Wprowadzona macierz ma postac: ')
disp(a)

fprintf('Wartosc maksymalna macierzy ma postac %1.0f \n' ,wart_max)
fprintf('Indeksy tego wyrazu maja postac: \n wiersz: %2.0f \n kolumna: %1.0f \n' ,indexw ,indexk) 


clear a