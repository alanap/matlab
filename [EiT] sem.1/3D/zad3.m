x=0:0.5:5;
y=0:0.5:5;

[x,y]=meshgrid(0:1:5);
z=x.^2-y.^2;
mesh(x,y,z)