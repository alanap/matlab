%ziarenko


n=1000;
r=sqrt(n);
d=137.51;

for i=1:n
    hold on
    r=sqrt(i);
    l=(pi*d*i)/180;
    plot(r*cos(l),r*sin(l),'o')
end

