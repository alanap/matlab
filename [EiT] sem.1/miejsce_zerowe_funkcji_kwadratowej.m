%Miejsca zerowe kwadratowego

function[x1, x2]=zero(a,b,c)

if a<=0
    rownanie_liniowe(b,c)
else
    
    delta=b^-4*(a*c)
    delta2=sqrt(delta)
    if delta>0
    disp('2 Rozwiazania')
    x1=(-b-delta2)/(2*a)
    x2=(-b+delta2)/(2*a)
    
    end
 
    if delta<=0
    disp('1 Rozwiazanie')
    x1=-b/2*a
    end
end
x1,x2