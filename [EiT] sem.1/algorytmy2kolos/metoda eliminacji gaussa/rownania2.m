A=input('Wprowadz macierz wspolczynnikow: ');
b=input('Wprowadz wektor wynikow: ');
[n,m]=size(A);
B=[A b'];
for p=1:m
    max=abs(B(p,p));
    imax=1;
    for i=p:m
        if max<=abs(B(i,p))
            max=B(i,p);
            imax=i;
        end
    end
    if max==0
        disp('macierz osobliwa'); 
		break;	
    end
    c=B(p,:);
    for i=1:n+1
        B(p,i)=B(imax,i);
        B(imax,i)=c(i);
    end
    for r=p+1:n
        mrp=B(r,p)/B(p,p);
        B(r,p)=0;
        for c=p+1:n+1
            B(r,c)=B(r,c)-mrp*B(p,c);    
        end
    end
end
C=B(1:n,1:n);
d=B(1:n,n+1);
x=podst(C,d);
disp(x);