function x=podst(A,b);
[n,m]=size(A);
x(m)=b(m)/A(n,m);
for i=m-1:-1:1
	suma=0;
	for j=i+1:n
		suma=suma+A(i,j)*x(j);
	end
	x(i)=(b(i)-suma)/A(i,i);
end
