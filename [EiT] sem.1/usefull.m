%USEFULL COMMANDS

%tworzenie macierzy o 2 wierszach z kolumnami 10
a=[1:1:10;2:2:20]

%macierze jednostkowe, zerowe etc.
>> a=zeros(2,2)
>> a=ones(2,2)
>> a=eye(4,6)   a=eye(n)

%odwolanie sie do elementu macierzy
a(2,1)

%usuwanie 2 wiersza macierzy a
A(2,:)=[]

%rozmiar macierzy a
>> [n,m]=size(A)

%algreba liniowa.
det(A) - obliczanie wyznacznika macierzy A
diag(A) - wyznaczanie elementow lezacych na przekatnej macierzy A

%fprintf
>> fprintf('wplacasz %10.2f po %2i, masz %10.3f \n', a, a, a)