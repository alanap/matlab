%Kreslenie wykresu f-kcji
min=input('Podaj wartosc minimalna dziedziny funkcji: ');
max=input('Podaj wartosc maxymalna dziedziny funkcji: ');


x=min:0.1:max;
wzorek=input('Podaj wzor fukcji jednej zmiennej f(x)= ' , 's');
y=eval(wzorek);
plot(x,y)

title('Wykres funkcji f(x)')
xlabel('argumenty')
ylabel('wartosci')
grid
legend('f(x)')