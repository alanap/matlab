function x=podst(A,b)
[n,n]=size(A)
x(n)=b(n)/A(n,n);

for i=n-1:-1:1
    suma=0;
    for j=i+1:n
        suma=suma+A(i,j)*x(j);
    end
end

