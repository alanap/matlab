%4138. Harry and big doughnuts
%https://pl.spoj.pl/problems/DOUGHNUT/

%c-     ilosc kotow
%k-     w [kg] udzwig jasia
%w-     w [kg] waga paczka

%Pozwolilem sobie dodac warunek : k,w>0
%   Oraz dodalem wiersze ktore zaokraglaja liczbe do najblizszej lp.
%   calkowitej jesli ta nie nalezy do tego zbioru.
%c, k oraz w (1 <= c, k, w <= 100)

clear c
clear k
clear w
clear t

fprintf('M�ody Harry zosta� poproszony o kupienie karmy dla kot�w przez swoj� s�siadk� \n star�, dziwn� pani�, kt�ra by�a w�a�cicielk� wielu kot�w. Ale koty te� by�y \n dziwne i jada�y tylko p�czki. Tote� s�siadka chcia�a aby Harry przyni�s� jej \n dok�adnie po jednym p�czku dla ka�dego kota - a posiada�a ich c. Harry mia� \n ze sob� plecak, ale �e by� tylko ma�ym ch�opcem, potrafi� ud�wign�� jedynie \n k kilogram�w. Harry wiedzia�, �e ka�dy p�czek wa�y w kilogram�w (du�e koty, du�e p�czki). \n Pom� Harremu zdecydowa� czy powinien i�� do supermarketu i kupi� �arcie, czy si� po \n prostu podda� i pomarzy� o odrobinie magii... \n \n')

t=input('Podaj ile testow chcesz wykonac: ');
while t<=0
    disp('Podaj lp. dodatnia')
    t=input('Podaj ile testow chcesz wykonac: ');
end



for i=1:t


    c=input('Podaj ilosc kotow sasiadki Harrego: ');
    c=round(c);
    while c<1
        disp('Liczba kotow musi byc  c>=1')
        c=input('Podaj ilosc kotow sasiadki Harrego: ');
    end


    k=input('Podaj udzwig Jasia w [kg]: ');
    while k>100 | k<=0
        disp('Udzwig Jasia musi spelniac warunek 0<k<=100')
        k=input('Podaj udzwig Jasia w [kg]: ');
    end



    w=input('Podaj wage jednego paczka: ');
    while w>100 | w<=0
        disp('Waga jednego paczka musi spelniac warunek 0<w<100')
        w=input('Podaj wage jednego paczka: ');
    end
    disp('---------------------------------')
    %Obliczanie wagi prowiantu dla kotow sasiadki
    prowiant=c*w;

    %Sprawdzanie czy prowiant nie zlamie kregoslupa Harrego
    if prowiant>k
        wynik(i)=0;
    elseif prowiant<=k
        wynik(i)=1;
    end


end


%Przeksztalcenie wartosci logicznych 0, 1 na wartosci tekstowe "Yes", "No"
disp('Koncowe wyniki')
for i=1:t
    if wynik(i)==0
        disp('No')
    elseif wynik(i)==1
        disp('Yes')
    end
end


