%3326. Warunek w tablicy
%https://pl.spoj.pl/problems/PASTAB2/
clear a
clear o
clear n
clear b

n=input('Podaj ile liczb chcesz wczytac: ');
while n<=0
    disp('Podaj lp. dodatnia')
    n=input('Podaj ile liczb chcesz wczytac: ');
end

for i=1:n
    fprintf('Podaj liczbe nr. %0.0f' ,i)
    a(i)=input(': ');
end
disp('Tablica ma postac: ')
disp(a')

o=menu('Jakie liczby chcesz wyswietlic?', '">" wieksze od...', '"<" mniejsze od...', '"=/=" rozne od...');
%                                         1     2    4



warunkowa=input('Teraz podaj liczbe ktora ma byc czescia warunku: ');


j=1; %Jesli j pozostanie nie zmienione oznaczacz bedzie ze algorytm z tablicy nie wybierze zadnych liczb.
switch o
    case 1      % >
        for i=1:n
            if a(i)>warunkowa
                b(j)=a(i);
                j=j+1;
            end
        end
        
        if j==1
            disp('Takich liczb nie mozna wyswietlic')
        elseif j>1
            fprintf('Wyswietle liczby wieksze od %0.0f \n' ,warunkowa)
            disp(b')
        end
            
        
        
        
    case 2      % <
        for i=1:n
            if a(i)<warunkowa
                b(j)=a(i);
                j=j+1;
            end
        end
        if j==1
            disp('Takich liczb nie mozna wyswietlic')
        elseif j>1
            fprintf('Wyswietle liczby mniejsze od %0.0f \n' ,warunkowa)
            disp(b')
        end
        
    case 3      % =/=
        for i=1:n
            if a(i)~=warunkowa
                b(j)=a(i);
                j=j+1;
            end
        end
        if j==1
            disp('Takich liczb nie mozna wyswietlic')
        elseif j>1
            fprintf('Wyswietle liczby rozne od %0.0f \n' ,warunkowa)
            disp(b')
        end
        
end














