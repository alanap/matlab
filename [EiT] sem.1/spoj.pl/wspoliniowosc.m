%1240. Wsp�liniowo�� punkt�w
%https://pl.spoj.pl/problems/JWSPLIN/
clear a
clear suma
n=3;
m=2;
kolumna(1)='X';
kolumna(2)='Y';
wiersz(1)='A';
wiersz(2)='B';
wiersz(3)='C';

disp('Algorytm sprawdzania, czy punkty sa wspolliniowe')

t=input('Ile razy wykonac probe: ');
if t>0
    for proba=1:t
        fprintf('Podaj wspolrzedne punktow A,B,C, dla proby nr. %0.0f \n' ,proba)
        for i=1:n
            for j=1:m
                fprintf('Wartosc: '),fprintf(kolumna(j)),fprintf(' dla punktu '),fprintf(wiersz(i))
                a(i,j)=input(': ');
            end
        end

        suma(proba)=(a(1,1)*a(2,2))+(a(2,1)*a(3,2))+(a(3,1)*a(1,2))-(a(3,1)*a(2,2))-(a(2,1)*a(1,2))-(a(1,1)*a(3,2));
    end
else
    disp('Liczba prob ma byc dodatnia')
end


disp('Wspolniniowosc punktow w kolejnych probach.')
for proba=1:t
    if suma(proba)==0
        fprintf('Proba nr. %0.0f: ' ,proba)
        fprintf('Tak \n')
    else
        fprintf('Proba nr. %0.0f: ' ,proba)
        fprintf('Nie \n')
    end
end

       
