%542. Potęgowanie
%https://pl.spoj.pl/problems/T_POTEGA/
clear wynik

t=input('Podaj ilosc prob: ');
t=round(t);
%Warunek z tresci
if t>0
    for i=1:t
        fprintf('PROBA NR. %0.0f \n' ,i)
        disp('Algorytm wykona nastepujace dzialanie: x^n mod p')
        x=input('Podaj x: ');
        while x<1 | x>100
            disp('Podana przez Ciebie liczba nie jest prawidlowa')
            x=input('Podaj x: ');
        end
        
        n=input('Podaj n: ');
        while n<0 | n>10000
            disp('Podana przez Ciebie liczba nie jest prawidlowa')
            n=input('Podaj n: ');
        end
        p=input('Podaj p: ');
        while p<2 | p>100
            disp('Podana przez Ciebie liczba nie jest prawidlowa')
            p=input('Podaj p: ');
        end
        
        wynik(i)=mod(x^n,p);
    end
    
        
else
    disp('Warunki zadanie nie sa spelnione, jak chcesz wykonac 0 prob?')
    break
end
disp('Wyniki dzialan w kolejnych probach wynosza, kolejno: ')
disp(wynik')
