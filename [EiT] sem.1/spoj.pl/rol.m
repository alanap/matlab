%1262. ROL (k)
%https://pl.spoj.pl/problems/PP0602D/

clear a
clear b
n=input('Podaj ilosc elementow wektora: ');
while n<=0
    disp('Liczba elementow musi byc dodatnia')
    n=input('Podaj ilosc elementow wektora: ');
end

k=input('Podaj krok z jakim algorytm przesunie kolejne elementy: ');
while k<0
    disp('Liczba krokow musi byc dodatnia')
    k=input('Podaj krok z jakim algorytm przesunie kolejne elementy: ');
end
    
if k>n
    disp('Liczba krokow nie moze byc wieksza od ilosc elementow wektora')
    while k>n
        k=input('Podaj krok z jakim algorytm przesunie kolejne elementy: ');
    end
end

a=1:1:n;




for i=k+1:n
    indexb=i-k;
    b(indexb)=a(i); 
end

for i=1:k
    b(n-k+i)=a(i);
end

disp('Wektor wprowadzony ma postac: '), disp(a)
disp('Wektor z przesunietymi elementami ma postac: '), disp(b)


    