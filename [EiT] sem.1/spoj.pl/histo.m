%3328. Histogram z liczb
%https://pl.spoj.pl/problems/PASTAB4/
clear a

n=input('Podaj ile elementow chcesz wczytac: ');
while n<=0
    disp('Ilosc elementow musi byc dodatnia: ')
    n=input('Podaj ile elementow chcesz wczytac: ');
end


for i=1:n
    fprintf('Podaj wartosc elementu: %0.0f' ,i)
    a(i)=input(': ');
end

hist(a,5*n)
ylabel('Ilosc liczb')
xlabel('Liczba')
title('Histogram z liczb')
grid
