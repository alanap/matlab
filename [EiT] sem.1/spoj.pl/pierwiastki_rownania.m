%969. R�wnanie kwadratowe
%https://pl.spoj.pl/problems/ROWNANIE/
clear a
clear b
clear c
clear x1
clear x2
clear liczba_rozwiazan


t=input('Ile razy program ma wyznaczyc liczb� pierwiastk�w rzeczywistych r�wnania kwadratowego: ');
t=round(t);
%Warunek z tresci
if t>0
    for i=1:t
    
    
        a=input('Podaj a: ');
        b=input('Podaj b: ');
        c=input('Podaj c: ');

        if a<=0
            if b>0
            x1=-c/b;
            fprintf('1 rozwiazanie: x1= %0.2f  \n' ,x1)
                liczba_rozwiazan(i)=1;
            else
            disp('Brak miejsca zerowego funkcji')
        end




        else

            delta=b^2-4*(a*c);
            delta2=sqrt(delta);
            if delta>0
            x1=(-b-delta2)/(2*a);
            x2=(-b+delta2)/(2*a);
            fprintf('2 rozwiazania: x1= %0.2f , x2= %0.2f \n' ,x1, x2)
                liczba_rozwiazan(i)=2;

            elseif delta<=0
            x1=-b/2*a;
            fprintf('1 rozwiazanie: x1= %0.2f  \n' ,x1)
                liczba_rozwiazan(i)=1;
            end
        end
        
        
        
    end
    
else
    disp('Warunki zadanie sa niespelnione')
end

disp('Liczba rozwiazan rownan w kolejnych probach wynosi: ')
disp(liczba_rozwiazan)