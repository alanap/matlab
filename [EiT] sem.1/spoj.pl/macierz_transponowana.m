%1042. Transponowanie macierzy
%https://pl.spoj.pl/problems/TRN/
clear a
clear b
n=input('Podaj ilosc wierszy: ');
while n<1
    disp('Ilosc wierszy musi byc dodatnia');
    n=input('Podaj ilosc wierszy: ');
end

m=input('Podaj ilosc kolumn: ');
while m<1
    disp('Ilosc kolumn musi byc dodatnia');
    m=input('Podaj ilosc kolumn: ');
end


for i=1:n
    for j=1:m
        fprintf('Podaj element macierzy o wspolrzednych: (%0.0f , %0.0f): ' ,i ,j)
        a(i,j)=input('');
    end
end
disp('Wprowadzona macierz ma postac: ')
disp(a)


for i=1:n
    for j=1:m
        b(j,i)=a(i,j);
    end
end 
disp('Maccierz transponowana ma postac: ')
disp(b)