%miejsca zerowe z wykres

xmin=0;
xmax=20;
epsilon=0.001;

c=(xmax+xmin)/2;
y=c^3-30*c^2+2552

while abs(y)>epsilon
    if y<0
        xmin=xmin
        xmax=c
    elseif y>0
        xmin=c
        xmax=xmax
    end
    c=(xmax+xmin)/2;
    y=c^3-30*c^2+2552
    
end

fprintf('Funkcja ma miejsce zerowe dla x=%0.4f \n' ,c)
