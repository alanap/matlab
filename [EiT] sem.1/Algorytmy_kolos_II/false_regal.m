%Szukanie miejsca zerowego

a=-2.4;
b=-1.6;

fa=exp(a)-2-a;
fb=exp(b)-2-b;

c=b-((fb)*(b-a))/(fb-fa);

fc=exp(c)-2-c;

while abs(fc)>0.0001
    if fa*fc<0
        a=a;
        b=c;
        fb=exp(b)-2-b;
    end
    c=b-((fb)*(b-a))/(fb-fa);
    fc=exp(c)-2-c;
end
disp(c)