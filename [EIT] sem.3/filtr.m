[code]function [l,m] = filtr(wp,ws,ap,as)
Op = 1;
Os = ws/wp; 
ks = Op/Os;
kd = sqrt((10^(ap/10)-1)/(10^(as/10)-1));
n = abs(log(kd))/abs(log(ks));
n = ceil(n);
A=1;
if (mod(n,2)==0)
    for (k=1:n/2)
        A = conv(A, [1 2*cos((2*k-1)*pi/(2*n)) 1]);
    end
else
    for (k=1:(n-1)/2)
        A = conv(A, [1 2*cos(k*pi/n) 1]);
    end
    A = conv([0 1 1], A);
end




normalize = (10^(ap/10)-1)^(1/2*n);
dl = length(A);

for j=1:dl-1
    A(j) = A(j/wp^(dl-j));
end
disp('A Iz: ');
A

disp('Wspolczynnik f iz: ');
n
LOL = poly(A);[/code]