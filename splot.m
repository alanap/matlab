function f=splot(a,b)
x=length(a);
y=length(b);
for i=1:y
    for j=1:x
        t(i,j+i-1)=a(j)*b(i);
    end
end
for i=1:x+y-1
    f(i)=sum(t(:,i));
end

%splot(a,b); gdzie a,b to wektory