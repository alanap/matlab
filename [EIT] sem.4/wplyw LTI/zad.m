clear; clc;
Fs = 1;
wp = (0.5*pi)/(2*pi);
ws = (0.5*pi)/(2*pi)*1.5;
[n f0 m0 w] = remezord([wp ws],[3 0],[0.01 0.01],Fs); %pierwszy [] od Hz do Hz
b = remez(n,f0,m0,w);
 
s = randn(1,5000); %losowy gauss 400 i 40000
y = filter(b,1,s); %FIR
 
% freqz - ch. czest
% widmo wej
% widmo wyj
% co to jest f0, m0, w
 
[H1,w1] = freqz(b,1,1024,Fs);
[H2,w2] = freqz(s,1,1024,Fs);
[H3,w3] = freqz(y,1,1024,Fs);
 
subplot(3,1,1);
plot(w1,abs(H1)); grid;
subplot(3,1,2);
plot(w2,abs(H2)); grid;
subplot(3,1,3);
plot(w3,abs(H3)); grid;
 
[Pxx,f1] = psd(s,length(s),Fs,hanning(length(s)),0);
[Pyy,f2] = psd(y,length(y),Fs,hanning(length(s)),0);
 
figure;
subplot(3,1,1);
plot(f1,Pxx); grid;
subplot(3,1,2);
plot(w1,abs(H1).^2); grid;
subplot(3,1,3);
plot(f2,Pyy); grid;
f0
m0
w
