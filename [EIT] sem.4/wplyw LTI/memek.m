clear;
clc;
Fs=1;
[n,f0,m0,w]=remezord([0.25 1.5*0.25],[3 0],[0.01 0.01],Fs);
b=remez(n,f0,m0,w);
s=randn(1,5000);  
y=filter(b,1,s);
[h1,w1]=freqz(b,1,1024,Fs);
[h2,w2]=freqz(s,1,1024,Fs);
[h3,w3]=freqz(y,1,1024,Fs);
subplot(6,1,1); plot(w1,abs(h1)); grid;
subplot(6,1,2); plot(w2,abs(h2)); grid;
subplot(6,1,3); plot(w3,abs(h3)); grid;
[Pss,f1]=psd(s,length(s),Fs,hanning(length(s) ),0);
[Pyy,f2]=psd(y,length(y),Fs,hanning(length(s) ),0);
subplot(6,1,4); plot(f1,Pss); grid;
subplot(6,1,5); plot(w1,abs(h1).^2); grid;
subplot(6,1,6); plot(f2,Pyy); grid;
