zp=0.01;
zz=0.1;
fp=200;
Wp=2*pi*fp;
fs=300;
Ws=2*pi*fs;
eps=(2*pi*sqrt(zp))/(1-zp);
Ap=-10*log10(1/((eps^2)+1));
A=(1+zp)/zz;
As=-10*log10(1/A^2);
[n,Wn]=buttord(Wp,Ws,Ap,As,'s');
[b,a]=butter(n,Wn,'s');
[c,d]=freqs(b,a);
subplot(2,1,1), plot(d,abs(c')), grid;
subplot(2,1,2), plot(d,angle(c')), grid;

