function [result] = row (a,b,x,y_begin)

a_length = length (a);
b_length = length (b);
x_length = length (x);
y_begin_len = length (y_begin);

d = zeros (1, x_length + a_length -1); 
x_temp = zeros (1, x_length+2*(a_length-1));
y_temp = zeros (1, x_length + a_length -1 + b_length-1 + y_begin_len);
y = zeros (1, x_length + a_length -1 + b_length-1);

for n = 1 : x_length 
    x_temp(n+a_length-1) = x(n);
end

for n = 1 : y_begin_len 
    y_temp(y_begin_len-n+1) = y_begin(n);
end

for n = 1 : x_length + a_length - 1
    for m = 1 : a_length
        d(n) = d(n) + x_temp(n-m+a_length)*a(m);
    end
end

for n = 1 : length(y_temp) - y_begin_len
    if ( n <= length(d) )
        y_temp(y_begin_len+n) = d(n)*b(1);
    end
    if (b_length > 1)
        for m = 1 : b_length-1
            y_temp(y_begin_len+n) = y_temp(y_begin_len+n) - y_temp(y_begin_len+n-m)*b(m+1);
            
        end
    end
end

for n = 1 : length (y) 
    y(n) = y_temp(n+y_begin_len);
end

result = y;