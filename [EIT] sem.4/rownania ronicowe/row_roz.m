function [wynik] = row_roz (a,b,x,y_poc)

%wielokana�owy sygna� impulsu jednostkowego u�ywaj�c do tego celu dw�ch funkcji Matlaba zeros i ones oraz 
%operator dwukropka. Przyk�adowo utworzono sze�cioelementowy wektor kolumnowy, kt�rego pierwszy element 
%jest r�wny 1, a pozosta�e pi�� element�w s� zerami:
%a=[1; zeros(5,1)];  
%Do powielenia wektora kolumnowego w macierz (bez dokonywania mno�enia), u�yto w Matlabie operatora (:), 
%oraz funkcji ones.
%c=a(:,ones(1,3));

x_len = length (x);
a_len = length (a);
b_len = length (b);
y_poc_len = length (y_poc);

d = zeros (1, x_len + a_len -1); %macie� d
x_temp = zeros (1, x_len+2*(a_len-1));
y_temp = zeros (1, x_len + a_len -1 + b_len-1 + y_poc_len);
y = zeros (1, x_len + a_len -1 + b_len-1);

for n = 1 : x_len %wype�nianie x_temp przesuni�tym x
    x_temp(n+a_len-1) = x(n);
end

for n = 1 : y_poc_len %wstawienie warunk�w pocz�tkowych do y_temp
    y_temp(y_poc_len-n+1) = y_poc(n);
end

for n = 1 : x_len + a_len - 1
    for m = 1 : a_len
        d(n) = d(n) + x_temp(n-m+a_len)*a(m);
    end
end

for n = 1 : length(y_temp) - y_poc_len
    if ( n <= length(d) )
        y_temp(y_poc_len+n) = d(n)*b(1);
    end
    if (b_len > 1) %je�eli wpisano jaki� wektor b, to liczymy...
        for m = 1 : b_len-1
            y_temp(y_poc_len+n) = y_temp(y_poc_len+n) - y_temp(y_poc_len+n-m)*b(m+1);
            
        end
    end
end

for n = 1 : length (y) %przepisanie wynik�w do y
    y(n) = y_temp(n+y_poc_len);
end

wynik = y;