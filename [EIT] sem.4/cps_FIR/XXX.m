clear; clc;
Fs=2192; %ilosc probek
[n,f0,m0,w]=remezord([190 300],[1 0],[0.1 0.1],Fs);
disp('stopien filtru: '); disp(n);
b = remez (n, f0, m0, w);
[H,f]=freqz(b,1,512,Fs);
subplot(3,1,1); 
plot(f,abs(H)); grid;
subplot(3,1,2); 
plot(f,unwrap(angle(H))); grid;
subplot(3,1,3); 
impz(b); grid;

%load handel.mat;
%x=y;
%y1=filter(b,1,x);
%wavplay(y,Fs,'sync');
%wavplay(y1,Fs);
