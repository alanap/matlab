fs=30;
fx=2;
N=200;
t=[0:1/fs:(N-1)/fs];
y=sin(2*pi*fx*t);
f=fs*(0:length(y)-1/length(y));
Y=fft(y);
subplot(2,1,1);stem(y);
subplot(2,1,2); stem(f,abs(Y));
% funkcje okna
w=(window(@triang,N))'; %@hamming @bartlett
a=y.*w;
A=fft(a);
 subplot(4,1,1); stem(y); title('syngal podany');
    subplot(4,1,2); stem(w); title('ksztalt okna');
    subplot(4,1,3); stem(f,abs(Y)); title(' widmo sygnalu ');
    subplot(4,1,4); stem(f,abs(A)); title(' widmo sygnalu wycietego oknem ');
    
 