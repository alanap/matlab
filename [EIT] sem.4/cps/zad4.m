    N=95;
    fs=30;
    t=0:1/fs:(N-1)/fs;
    f1=3;
    s1=sin(2*pi*f1*t);
    f=fs*(0:length(s1)-1)/length(s1); 
    S1=fft(s1);


     subplot(2,1,1); stem(t,s1); title(' s1 '); %xlabel('argumenty'); ylabel('wartosci funkcji');
     subplot(2,1,2); stem(f,abs(S1)); title('DFT ( s1 )'); %xlabel('argumenty'); ylabel('wartosci funkcji');
