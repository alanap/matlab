    N=100;
    fs=100;
    t=0:1/fs:(N-1)/fs;
    f1=10;
    s1=sin(2*pi*f1*t);
    f2=20;
    s2=sin(2*pi*f2*t);
    f=fs*(0:length(s1)-1)/length(s1); 
    S1=fft(s1);
    S2=fft(s2);
    S12=fft(s1+s2);
    S21=fft(s1)+fft(s2);

    
    subplot(4,1,1); stem(f,abs(S1)); title('DFT ( s1 )'); %xlabel('argumenty'); ylabel('wartosci funkcji');
    subplot(4,1,2); stem(f,abs(S2)); title('DFT ( s2 )'); %xlabel('argumenty'); ylabel('wartosci funkcji');
    subplot(4,1,3); stem(f,abs(S12)); title('DFT ( s1+s2 )');% xlabel('argumenty'); ylabel('wartosci funkcji');
    subplot(4,1,4); stem(f,abs(S21)); title('DFT (s1) + DFT (s2)');% xlabel('argumenty'); ylabel('wartosci funkcji');
